# Sphaleron Event Generation for Herwig 7


## Prerequisites

This code has been confirmed to work with Herwig 7.2.3 and Python 3.8.10. 

## Installation

The repository should be cloned in the `Herwig-7.x.x/Contrib/sphalerons` directory where `Herwig-7.x.x` isa the Herwig source directory.

Then in `Herwig-7.x.x/Contrib` type `make`. This should create the `Makefile` in `Herwig-x.x.x/Contrib/sphalerons`.

You will then need to copy some code over from the instantons subdirectories as follows:

Copy contents of `Herwig-7.x.x/Contrib/instantons/Sampling` to `Herwig-7.x.x/Sampling`.
Copy contents of `Herwig-7.x.x/Contrib/instantons/Phasespace` to `Herwig-7.x.x/MatrixElement/Matchbox/Phasespace`.

Then go to Herwig-7.x.x and do:

`autoreconf -vi`

You will need to recompile after this. If you’ve already done that, you can just type e.g.

`make -j12; make -j12 install`

Then if you go back to the Herwig-7.x.x/Contrib/sphalerons you should be able to compile by typing:

`make`

## Example input file

There's an example input file:

PP13-Sphaleron-THR9-FRZ15-NB33-60-NSUBP50.in

You can read it and run it then via:

`Herwig read PP13-Sphaleron-THR9-FRZ15-NB33-60-NSUBP50.in`

`Herwig run PP13-Sphaleron-THR9-FRZ15-NB33-60-NSUBP50.run -N100`

To generate 100 events.

## References

If you use this code please cite [https://inspirehep.net/literature/1758687].

## Authors

A. Papaefstathiou, S. Plätzer, K. Sakurai.
