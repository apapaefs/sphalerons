// -*- C++ -*-
//
// This is the implementation of the non-inlined, non-templated member
// functions of the MESphaleron class.
//

#include <Python.h>
#include <string>
#include <algorithm>

#include "MESphaleron.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/Repository/UseRandom.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/Utilities/DescribeClass.h"
#include "ThePEG/Interface/Parameter.h"
#include "ThePEG/Interface/Switch.h"
#include "ThePEG/Utilities/Maths.h"

#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"

//#include "sphaleron_process_info_20-30.h"

using namespace Herwig;

MESphaleron::MESphaleron()
  : theNFamilies(3), theSphaleronScale(9000.*GeV), theSphaleronScaleMax(100000.*GeV), theNAdditionalMin(0), NPhotonMax(-1), NZMax(-1), NHiggsMax(-1), EvenNHiggs(false), BinomialNeutralBosons(true), BinomialGaugeBosons(true), Wmass(80.4), prefactor(1.6E-101), proctype(0), subsample(0), ProcessGeneration(0), NRandom(1), NumBosonParam(1), NumBosonParamA1(-21.497), NumBosonParamA2(0.590E-2), NumBosonParamB1(0.483E-1), NumBosonParamB2(-0.854E-6), AdditionalPSFactors(false), LOME(false), ReinstatePSFactor(false), prob_W(2./3.), prob_Z(1.-0.2223), minphotons(0), minhiggses(0), minzs(0), prob_H(1./16.), NWC_DAT_MAX(200), FreezeGaussianParamsScale(15000.*GeV), FreezeGaussianParams(true), EnforceUnitarityLimit(true), EnforceUnitarityLimitScale(1E4*GeV) {}

MESphaleron::~MESphaleron() {}

IBPtr MESphaleron::clone() const {
  return new_ptr(*this);
}

IBPtr MESphaleron::fullclone() const {
  return new_ptr(*this);
}

size_t MESphaleron::nOutgoing() const {
  return 10; //this should be set to the number of outgoing particles, not including the "additional" gauge/Higgs bosons, i.e. the number of quarks + leptons
}

vector<vector<int>> MESphaleron::gen_line(vector<int> ncints, vector<vector<int>> ncints_rem) const {
  vector<vector<int>> line;
  vector<int> line_tmp;
  
  for(int ncr = 0; ncr < ncints_rem.size(); ncr++) { 
    //first remove the numbers in ncints given in ncints_rem:
    vector<int> ncints_new = ncints;
    for(int ll = 0; ll < ncints_rem[ncr].size(); ll++) { 
      ncints_new.erase(std::remove(ncints_new.begin(), ncints_new.end(), ncints_rem[ncr][ll]), ncints_new.end());
    }
    
    for(int mm = 1; mm < ncints_new.size()-1; mm++) {
      for(int nn = mm+1; nn < ncints_new.size(); nn++) {
	if(ncints_new.size() < ncints.size()) { 
	  for(int rr = 0; rr < ncints_rem[rr].size(); rr++) {
	    line_tmp.push_back(ncints_rem[ncr][rr]);
	  }
	}
	line_tmp.push_back(ncints_new[0]);
	line_tmp.push_back(ncints_new[mm]);
	line_tmp.push_back(ncints_new[nn]);
	line.push_back(line_tmp);
	line_tmp.clear();
      }
    }
  }
 
  return line;
}




void MESphaleron::doinit() {
  
  cout << "Initializing" << endl;

  //the W boson mass
  PDPtr wboson = getParticleData(ParticleID::Wplus);
  Wmass = wboson->mass()/GeV;

  // number of additional particles
  theNAdditional = int(this->nAdditional());

  if(NPhotonMax!=-1) { NPhotonRed = theNAdditional - NPhotonMax; }
  if(NZMax!=-1) { NZRed = theNAdditional - NZMax; }
  if(NHiggsMax!=-1) { NHiggsRed = theNAdditional - NHiggsMax; }
  
  if(NPhotonMax>theNAdditional) { NPhotonRed = theNAdditional; }
  if(NZMax>theNAdditional) { NZRed = theNAdditional; }
  if(NHiggsMax>theNAdditional) { NHiggsRed = theNAdditional; }
  
  if(theNAdditionalMin > theNAdditional) { cerr << "theNAdditionalMin should be < theNAdditional" << endl; }

  //set the probability of Z instead of gamma according to the Weinberg angle
  prob_Z = 1-SM().sin2ThetaW();

  /* 
   * generate the colour connections for the quarks:
   * there are 9 quarks: we need to figure out all the possible
   * (unordered) combinations. 
   * note that later we will remove some combinations, since
   * identical particles cannot be colour-connected
   * as this would be zero due to total antisymmetry.
   */
  vector<int> ncints = {0, 1, 2, 3, 4, 5, 6, 7, 8};
  //generate a null vector of vector of integers
  vector<int> ncints_none = {};
  vector<vector<int>> ncints_null;
  ncints_null.push_back(ncints_none);
  
  vector<vector<int>> line1, line2;
  line1 = gen_line(ncints, ncints_null);
  line2 = gen_line(ncints, line1);
  linefull = gen_line(ncints, line2);

  //test: 
  /*  for(size_t ss = 0; ss < linefull.size(); ss++) {
      cout << "permutation " << ss << " : "; 
      for(size_t tt = 0; tt < linefull[ss].size(); tt++) {
      cout << linefull[ss][tt] << " ";
      }
      cout << endl;
      }*/
  
  // if the process generation will take place according to the hard-coded processes
  if(ProcessGeneration == 1) {
    //fill_processes();
    fill_processes_dat("./sphaleron_process_info_200.dat");
  }  
}


multimap<tcPDPair,tcPDVector> MESphaleron::processes() const {

  cout << "Processes being generated" << endl;

  tcPDPtr Z = getParticleData(ParticleID::Z0);
  tcPDPtr gamma = getParticleData(ParticleID::gamma);
  tcPDPtr H = getParticleData(ParticleID::h0);
  
  //the processmap to return 
  multimap<tcPDPair,tcPDVector> processmap;

  /* how many processes in the .dat file? */
  NWC_DAT_MAX = 200;

  //the vectors that hold the outgoing partons
  tcPDVector multi_outgoing_charged; //fermions and Ws
  tcPDVector multi_outgoing_bosons; //all neutral bosons
    
  tcPDVector multi_outgoing_z; //z bosons
  tcPDVector multi_outgoing_higgs; //higgs bosons
  tcPDVector multi_outgoing_gamma; //gammas

  tcPDVector multi_outgoing; //all particles
  
  //the pair that holds the incoming partons
  tcPDPair multi_incoming;

  //the maximum number of additional gauge bosons/Higgses/photons
  int ngen = 0, ntot = 0;

  //Python script
  PyObject *pName, *pModule, *pDict, *pFunc, *pValue, *presult, *pitem;
  string python_file = "sphaleron_processes";
  
  string python_processes = "";
  if(proctype == 1) { //MultiGauge
    python_processes = "process_string_BC";
  }
  else if(proctype == 0) {
    python_processes = "process_string";
  }
  else if(proctype == 2) {
    python_processes = "process_string_light";
  }
  
  // Set PYTHONPATH TO working directory
  setenv("PYTHONPATH",".",1);
  // Initialize the Python Interpreter
  Py_Initialize();
  // Build the name object
  //pName = PyString_FromString(python_file.c_str()); // PYTHON 2
  pName = PyUnicode_DecodeFSDefault(python_file.c_str()); // PYTHON 3
  
  // Load the module object
  pModule = PyImport_Import(pName);
  Py_DECREF(pName);
  // pDict is a borrowed reference 
  pDict = PyModule_GetDict(pModule);

  // pFunc is also a borrowed reference 
  //pFunc = PyDict_GetItemString(pDict, python_processes.c_str());
  pFunc = PyObject_GetAttrString(pModule, python_processes.c_str()); // PYTHON 3
  
  // generate limited processes for debugging purposes
  if (PyCallable_Check(pFunc)) {
    cout << "generating proccesses with = (" << theNAdditionalMin << ", " << theNAdditional << ") additional particles" << endl;
    cout << "\tw z γ h : #" << endl;
    
    /*
     * generate arrays of possible combinations
     */
    vector<int> NW, NZ, NH, NG;
    for(int numws = 0; numws <= theNAdditional; numws++) {
      for(int numzs = 0; numzs <= theNAdditional-numws; numzs++) {
	for(int numphotons = 0; numphotons <= theNAdditional-numzs-numws; numphotons++) {
	  for(int numhiggs = 0; numhiggs <= theNAdditional-numzs-numphotons-numws; numhiggs++) {
	    if(numhiggs + numphotons + numzs + numws < int(theNAdditionalMin)) { continue; }
	    if(numphotons > NPhotonMax || numphotons < minphotons) { continue; }
	    if(numzs > NZMax || numzs < minzs) { continue; }
	    if(numhiggs%2 != 0 && EvenNHiggs) { continue; }
	    if(numhiggs > NHiggsMax || numhiggs < minhiggses) { continue; }
	    NW.push_back(numws);
	    NZ.push_back(numzs);
	    NH.push_back(numhiggs);
	    NG.push_back(numphotons);
	    cout << "\t" << numws << " " << numzs << " " << numphotons << " " << numhiggs << endl;

	  }//end of loop over higgses
	}//end of loop over photons
      } //end of loop over zs
    } // end of loop over ws

    size_t nproduced = NW.size();
    int nzc = 0, nwc = 0, nhc = 0, ngc = 0;
    vector<string> split;					
    cout << "generated " << nproduced << " proccesses (no flavours)" << endl;
    for(int nn = 0; nn < nproduced; nn++) {
      nwc = NW[nn];
      nzc = NZ[nn];
      ngc = NG[nn];
      nhc = NH[nn];
      for(int iz = 0; iz < nzc; iz++) multi_outgoing_z.push_back(Z);
      for(int ia = 0; ia < ngc; ia++) multi_outgoing_gamma.push_back(gamma);
      for(int ih = 0; ih < nhc; ih++) multi_outgoing_higgs.push_back(H);
      
      multi_outgoing_bosons.insert(multi_outgoing_bosons.begin(), multi_outgoing_z.begin(), multi_outgoing_z.end());
      multi_outgoing_bosons.insert(multi_outgoing_bosons.begin(), multi_outgoing_gamma.begin(), multi_outgoing_gamma.end());
      multi_outgoing_bosons.insert(multi_outgoing_bosons.begin(), multi_outgoing_higgs.begin(), multi_outgoing_higgs.end());
      
      size_t nprocs;
      //call the python script.
      if(ProcessGeneration == 0) { 
	pValue=Py_BuildValue("(i)",nwc);
	PyErr_Print();
	presult=PyObject_CallObject(pFunc,pValue);
	nprocs = PyList_Size(presult);
      }
      else if(ProcessGeneration == 1) { //use the internal pre-defined array
	if(nwc < NWC_DAT_MAX+1) nprocs = process_array[nwc].size();
	/* 
	 * the data file only contains up to 100 processes:
	 * revert back to the python script beyond that
	 */
	if(nwc > NWC_DAT_MAX) {
	  pValue=Py_BuildValue("(i)",nwc);
	  PyErr_Print();
	  presult=PyObject_CallObject(pFunc,pValue);
	  nprocs = PyList_Size(presult);
	}
      }

      if(subsample == 1) {
	//pick a single random process. 
	int randproc = UseRandom::irnd(0,nprocs);
	vector<string> split;
	if(ProcessGeneration == 0) {
	  pitem = PyList_GetItem(presult, randproc);
	  //split = StringUtils::split(PyString_AsString(pitem)," "); // PYTHON 2
	  split = StringUtils::split(PyBytes_AsString(pitem)," "); // PYTHON 3
	} else if(ProcessGeneration == 1) {
	  if(nwc < NWC_DAT_MAX+1) split = StringUtils::split(process_array[nwc][randproc]," ");
	  if(nwc > NWC_DAT_MAX) {
	    pitem = PyList_GetItem(presult, randproc);
	    //split = StringUtils::split(PyString_AsString(pitem)," "); // PYTHON 2
	    split = StringUtils::split(PyBytes_AsString(pitem)," "); // PYTHON 3

	  }
	}

	string split_no_multiplicity = "";
	for(unsigned int tt = 1; tt < 10; tt++) { split_no_multiplicity += std::to_string(-stoi(split[tt])); split_no_multiplicity += " "; }
	colour_multiplicity_map[split_no_multiplicity] = stod(split[0]);
	multi_incoming = make_pair(getParticleData(-stoi(split[1])), getParticleData(-stoi(split[2])));
	for(unsigned int ss = 3; ss < split.size(); ss++) multi_outgoing_charged.push_back(getParticleData(-stoi(split[ss])));
	multi_outgoing.insert(multi_outgoing.begin(), multi_outgoing_bosons.begin(), multi_outgoing_bosons.end());
	multi_outgoing.insert(multi_outgoing.begin(), multi_outgoing_charged.begin(), multi_outgoing_charged.end());
	processmap.insert(make_pair(multi_incoming,multi_outgoing));
	multi_outgoing.clear(); multi_outgoing_charged.clear();
	ngen++;
      }
      else if(subsample == 2) {
	vector<int> randproc_vector;
	for(int uu = 0; uu < NRandom; uu++) {
	  //pick proccesses at random up to NRandom. 
	  int randproc = UseRandom::irnd(0,nprocs);
	  while(std::find(randproc_vector.begin(), randproc_vector.end(), randproc) != randproc_vector.end()) randproc = UseRandom::irnd(0,nprocs);
	  randproc_vector.push_back(randproc);
	  if(ProcessGeneration == 0) {
	    pitem = PyList_GetItem(presult, randproc);
	    //split = StringUtils::split(PyString_AsString(pitem)," "); // PYTHON 2
	    split = StringUtils::split(PyBytes_AsString(pitem)," "); // PYTHON 3

	  } else if(ProcessGeneration == 1) {
	    if(nwc < NWC_DAT_MAX+1) split = StringUtils::split(process_array[nwc][randproc]," ");
	    if(nwc > NWC_DAT_MAX) {
	      pitem = PyList_GetItem(presult, randproc);
	      //split = StringUtils::split(PyString_AsString(pitem)," "); //PYTHON 2
	      split = StringUtils::split(PyBytes_AsString(pitem)," "); // PYTHON 3

	    }
	  }
	  string split_no_multiplicity = "";
	  for(unsigned int tt = 1; tt < 10; tt++) { split_no_multiplicity += std::to_string(-stoi(split[tt])); split_no_multiplicity += " "; }
	  colour_multiplicity_map[split_no_multiplicity] = stod(split[0]);
	  multi_incoming = make_pair(getParticleData(-stoi(split[1])), getParticleData(-stoi(split[2])));
	  for(unsigned int ss = 3; ss < split.size(); ss++) multi_outgoing_charged.push_back(getParticleData(-stoi(split[ss])));
	  multi_outgoing.insert(multi_outgoing.begin(), multi_outgoing_bosons.begin(), multi_outgoing_bosons.end());
	  multi_outgoing.insert(multi_outgoing.begin(), multi_outgoing_charged.begin(), multi_outgoing_charged.end());
	  processmap.insert(make_pair(multi_incoming,multi_outgoing));
	  multi_outgoing.clear(); multi_outgoing_charged.clear();
	  ngen++;
	}
      }
      else if(subsample == 0) { 
	for(unsigned int jj = 0; jj < nprocs; jj++) {
	  if(ProcessGeneration == 0) {
	    pitem = PyList_GetItem(presult, jj);
	    //split = StringUtils::split(PyString_AsString(pitem)," "); //PYTHON 2
	    split = StringUtils::split(PyBytes_AsString(pitem)," "); // PYTHON 3

	  } else if(ProcessGeneration == 1) {
	    if(nwc < NWC_DAT_MAX+1) split = StringUtils::split(process_array[nwc][jj]," ");
	    if(nwc > NWC_DAT_MAX) {
	      pitem = PyList_GetItem(presult, jj);
	      //split = StringUtils::split(PyString_AsString(pitem)," "); // PYTHON 2
	      split = StringUtils::split(PyBytes_AsString(pitem)," ");
	    }
	  }
	  
	  string split_no_multiplicity = "";
	  for(unsigned int tt = 1; tt < 10; tt++) { split_no_multiplicity += std::to_string(-stoi(split[tt])); split_no_multiplicity += " "; }
	  colour_multiplicity_map[split_no_multiplicity] = stod(split[0]);
	  // WARNING: the first number in the list is the multiplicity due to the different number of colour configs!
	  multi_incoming = make_pair(getParticleData(-stoi(split[1])), getParticleData(-stoi(split[2])));
	  for(unsigned int ss = 3; ss < split.size(); ss++) multi_outgoing_charged.push_back(getParticleData(-stoi(split[ss])));
	  multi_outgoing.insert(multi_outgoing.begin(), multi_outgoing_bosons.begin(), multi_outgoing_bosons.end());
	  multi_outgoing.insert(multi_outgoing.begin(), multi_outgoing_charged.begin(), multi_outgoing_charged.end());
	  processmap.insert(make_pair(multi_incoming,multi_outgoing));
	  multi_outgoing.clear(); multi_outgoing_charged.clear();
	  ngen++;
	}
      }
      
      cout << "\t" << nwc << " " << nzc << " " << ngc << " " << nhc << " : " << ngen << endl;
      ntot += ngen;
      ngen = 0;
      multi_outgoing_higgs.clear();
      multi_outgoing_bosons.clear();
      multi_outgoing_gamma.clear();
      multi_outgoing_z.clear();
    }
  Py_Finalize();
  cout << "\ttotal : " << ntot << endl;
   
  }
  return processmap;

}


double MESphaleron::binomialfunct(double prob, int N1, int N2) const {
  double bfac;
  bfac = pow(prob,N1) * pow((1-prob),N2) * factorial(N1+N2)/factorial(N1)/factorial(N2);
  return bfac; 
}

double MESphaleron::me2() const {
  
  if(sHat() < sqr(theSphaleronScale) || sHat() > sqr(theSphaleronScaleMax)) { return 0.; }
  
  count_bosons();

  double ME(1.);
  double n = _na + _nz + _nw + _nh;
  double vev = 246.;
  double sqrtshdouble=sqrt(sHat()/GeV/GeV);
  
  ME *= pow(prefactor,2);

  if(BinomialGaugeBosons) {
    ME *= binomialfunct(prob_W, _nw, _nz+_na);
  }

  if(BinomialNeutralBosons) {
    ME *= binomialfunct(prob_Z, _nz, _na);
  }

  if(BinomialHiggses) {
    ME *= binomialfunct(prob_H, _nh, _nw+_nz+_na);
  }

  /* 
   * Re-weight the ME according to the factors related to the LOME
   * approximation
   */
  
  if(LOME) { 
    ME *= pow(2,n) * pow(sqrt(sHat()/GeV/GeV)/vev, 2*n) * pow(std::tgamma(n + 103./12.)/std::tgamma(103./12.), 2) / factorial(_nh) / factorial(n-_nh);
  }
  
  /* calculate the additional phase-space factors 
   * for fermions and gauge bosons according to LOME
   */
  if(AdditionalPSFactors) {
    ME *= get_fermion_additional_PS_factor();
    ME *= get_gaugeboson_additional_PS_factor();
  }
  
  /*  
   * colour multiplicity factors
   */
  ME *= get_colour_multiplicity();

  /* 
   * if the type of gauge boson number parametrization is Gaussian (NumBosonParam==1):
   * reweight by: exp(-(nB-a)/b) where 
   * a = NumBosonParamA1 + NumBosonParamA2 * \sqrt(\hat{s}), b = NumBosonParamB1 + NumBosonParamB2 * \sqrt(\hat{s})
   */
  // get the number of gauge bosons
  double _nb = _na + _nz + _nw;
  
  if(NumBosonParam==1 || NumBosonParam==2) {
    double a(0.), b(0.);

    a = NumBosonParamA1 + NumBosonParamA2 * sqrtshdouble;
    b = NumBosonParamB1 + NumBosonParamB2 * sqrtshdouble;

    if(FreezeGaussianParams && sqrtshdouble > FreezeGaussianParamsScale/GeV) {
      a = NumBosonParamA1 + NumBosonParamA2 * (FreezeGaussianParamsScale/GeV);
      b = NumBosonParamB1 + NumBosonParamB2 * (FreezeGaussianParamsScale/GeV);
    }
    
    if(NumBosonParam==1)  { ME *= exp( - pow(_nb - a,2) / b )/sqrt(M_PI * b) ; }
    if(NumBosonParam==2) { ME *= exp( - pow(_nb - a,2) / b ) ; }
    
    //test:
    //cout << _nb << "\t" << a << "\t" << b << "\t" << exp( - pow(_nb - a,2) / b )/sqrt(M_PI * b) << endl;
  }

  // multiply the ME-squared by some Phase-space factors
  if(ReinstatePSFactor) {
    if(n>1) { 
      ME /= pow(M_PI/2.,n-1)*pow(sHat()/GeV/GeV,n-2)/factorial(n-1)/factorial(n-2);
    }
  }

  // enforce the unitarity limit, i.e. make the partonic cross section scale as 1/sqrt(shat)
  if(EnforceUnitarityLimit && sqrtshdouble > EnforceUnitarityLimitScale/GeV) {
    ME *= pow(EnforceUnitarityLimitScale/GeV,2)/(sHat()/GeV/GeV);
  }

  /*
   * Check if NaN:
   */
  if(isnan(ME)) { cout << "ME is nan!" << endl; }
  if(isnan(jacobian())) { cout << "Jacobian is nan!" << endl; }

  //cout << "ME=" << ME << endl;
  /* 
   * return the ME^2
   */ 
  return ME;
}

bool MESphaleron::is_antisymmetric(vector<int> partons, vector<int> line) const {
  //check if any two within the same line are identical particles
  for(int zz = 0; zz < 3; zz++) { //0, 1, 2 ; 3, 4, 5 ; 6, 7, 8
    if(partons[line[3*zz]] == partons[line[3*zz+1]] || partons[line[3*zz]] == partons[line[3*zz+2]] || partons[line[3*zz+1]]  == partons[line[3*zz+2]] )  {
      //cout << "identical partons colour-connected!" << endl;
      return true;
    }
  }
  
  return false;
}

bool MESphaleron::is_antisymmetric_light(vector<int> partons, vector<int> line) const {

  /* 
   * the version of the function that checks whether the chosen colour-source/sink combination is anti-symmetric: 
   * check only for colour-connected top-quarks
   */  
  //check if any two within the same line are identical particles
  for(int zz = 0; zz < 3; zz++) { //0, 1, 2 ; 3, 4, 5 ; 6, 7, 8
    if( (partons[line[3*zz]] == partons[line[3*zz+1]] || partons[line[3*zz]] == partons[line[3*zz+2]] || partons[line[3*zz+1]]  == partons[line[3*zz+2]]) && partons[line[3*zz]] == 6 )  {
      //cout << "identical partons colour-connected!" << endl;
      return true;
    }
  }
  
  return false;
}


list<BlobMEBase::ColourConnection> MESphaleron::colourConnections() const {
 
  //"res" contains all the colour connections
  list<BlobMEBase::ColourConnection> res;

  //the colourSource will contain all the colour connections that will enter "res", before returning it
  vector<BlobMEBase::ColourConnection> colourSource;

  if(proctype == 0 || proctype == 2) { // Sphaleron processes (0 for full quark content, 2 excluding c-s-b quarks)

    vector<int> partons = { int(mePartonData()[0]->id()), int(mePartonData()[1]->id()), int(mePartonData()[2]->id()), int(mePartonData()[3]->id()), int(mePartonData()[4]->id()), int(mePartonData()[5]->id()), int(mePartonData()[6]->id()), int(mePartonData()[7]->id()), int(mePartonData()[8]->id()) };

    /* set-up the colour source for the initial state + one parton in the final state
       for now, just pick the first permutation */
    //choose a colour connection at random:
    int li = UseRandom::irnd(0,linefull.size());
    //check that it's not antisymmetric (i.e. there are no identical particles colour-connected)
    if(proctype == 0) { 
      while(is_antisymmetric(partons,linefull[li])) {
	li = UseRandom::irnd(0,linefull.size());
	/*cout << "line chosen = " << li << ": ";
	  for(int zz = 0; zz < linefull[li].size(); zz++) {
	  cout << linefull[li][zz] << " ";
	  }*/
      }
      /*  cout << "final line chosen = " << li << ": ";
	  for(int zz = 0; zz < linefull[li].size(); zz++) {
	  cout << linefull[li][zz] << " ";
	  }
	  cout << endl;*/
    }
    else if(proctype == 2) while(is_antisymmetric_light(partons,linefull[li])) li = UseRandom::irnd(0,linefull.size());

    vector<BlobMEBase::ColourConnection> colourSource_tmp;
    BlobMEBase::ColourConnection colourSource_tmp_tmp;
    for(int jj = 0; jj < 3; jj++) { colourSource_tmp.push_back(colourSource_tmp_tmp); } 
    int cn(0);
    for(int zz = 0; zz < linefull[li].size(); zz++) {
      if(linefull[li][zz]==0 || linefull[li][zz]==1) {
	colourSource_tmp[cn].addColour(linefull[li][zz]);
      } else {
	colourSource_tmp[cn].addAntiColour(linefull[li][zz]);
      }
      if((zz+1)%3==0) {
	colourSource.push_back(colourSource_tmp[cn]);
	cn += 1;
      }
    }
    //insert all the colour sources 
    for(size_t i = 0; i < colourSource.size(); ++i) {
      res.push_back(colourSource[i]);
    }
  }
  else if(proctype == 1) { //Baryon-number-conserving multi-gauge boson processes
    vector<int> partons = { int(mePartonData()[0]->id()), int(mePartonData()[1]->id()), int(mePartonData()[2]->id()), int(mePartonData()[3]->id()) };
    
    vector<vector<int>> combinations1 = { {0, 2}, {0, 3} };
    vector<vector<int>> combinations2 = { {1, 3}, {1, 2} };
    //pick one of the two combinations at random
    int combo = UseRandom::irnd(0,1);
    //create the two colour lines
    BlobMEBase::ColourConnection colourLine1;
    BlobMEBase::ColourConnection colourLine2;
    colourLine1.addColour(combinations1[combo][0]);
    colourLine1.addColour(combinations1[combo][1]);
    //cout << "colourLine1 contains:" << combinations1[combo][0] << "->" << combinations1[combo][1] << endl;
    //cout << "colourLine2 contains:" << combinations2[combo][0] << "->" << combinations2[combo][1] << endl;
    colourLine2.addColour(combinations2[combo][0]);
    colourLine2.addColour(combinations2[combo][1]);
    //insert them into the result. 
    res.push_back(colourLine1);
    res.push_back(colourLine2);     
  }

  return res;
}

std::pair<tcPDPair,tcPDVector> MESphaleron::string_to_process(string procstring) const {
  std::pair<tcPDPair,tcPDVector> result;
  vector<string> split = StringUtils::split(procstring," ");
  /* testing:
  cout << "procstring = " << procstring << endl;
  cout << "initial state = " << split[0] << " " << split[1] << endl;*/
  return result; 
}

void MESphaleron::count_bosons() const {
  //reset number of boson counters
  _nw = 0; _nh = 0; _na = 0; _nz = 0;
  for(unsigned int pp = 0; pp < mePartonData().size(); pp++) {
    if(abs(mePartonData()[pp]->id())==25) { _nh++; }
    if(abs(mePartonData()[pp]->id())==24) { _nw++; }
    if(abs(mePartonData()[pp]->id())==23) { _nz++; }
    if(abs(mePartonData()[pp]->id())==22) { _na++; }
  }
  //testing:
  //cout << "_nh = " << _nh << " nw = " << _nw << " nz = " << _nz << " ngamma = " << _na << endl;
}

double MESphaleron::get_colour_multiplicity() const {
  double colourmult(1.0);
  string quarkstring = "";
  for(unsigned int pp = 0; pp < 9; pp++) {
    quarkstring += std::to_string(mePartonData()[pp]->id()); quarkstring += " ";
  }
  //testing:
  //cout << "colour_multiplicity_map[quarkstring]= " << colour_multiplicity_map[quarkstring] << endl;
  colourmult = colour_multiplicity_map[quarkstring];
    
  return colourmult;
}


double MESphaleron::get_fermion_additional_PS_factor() const {
  double fermion_psfactor = 1.;
  //find the fermions and get their energy
  for(unsigned int pp = 0; pp < mePartonData().size(); pp++) {
    if(abs(mePartonData()[pp]->id()) <= 6 ) fermion_psfactor *= meMomenta()[pp].e()/GeV;
  }
  return fermion_psfactor;
}

double MESphaleron::get_gaugeboson_additional_PS_factor() const {
  double boson_psfactor = 1.;
  //find the fermions and get their energy
  for(unsigned int pp = 0; pp < mePartonData().size(); pp++) {
    if(abs(mePartonData()[pp]->id())==24 || abs(mePartonData()[pp]->id())==23 || abs(mePartonData()[pp]->id())==22) {
      boson_psfactor *= 2* ( 4* pow(meMomenta()[pp].e()/GeV, 2) - pow(Wmass,2) ) / pow(Wmass,2) ;
    }
  }
  return boson_psfactor;
}

void MESphaleron::fill_processes_dat(string filestring) const {
  std::vector<string> subarray;
  string line;
  ifstream filestream(filestring);
  if (filestream.is_open()) {
    while (getline(filestream,line) )
      {
	//cout << "line=" << line.c_str() << endl;
	if(line.size() < 10) { /*cout << "clearing" << subarray.size() << endl;*/ process_array.push_back(subarray); subarray.clear(); continue; } 
	subarray.push_back(line);
      }
    filestream.close();
  }
}


// If needed, insert default implementations of virtual function defined
// in the InterfacedBase class here (using ThePEG-interfaced-impl in Emacs).
void MESphaleron::persistentOutput(PersistentOStream& os) const {
  os << theNFamilies << ounit(theSphaleronScale, GeV) << ounit(theSphaleronScaleMax, GeV)<< theNAdditional << theNAdditionalMin << NPhotonMax << NZMax << NHiggsMax << NPhotonRed << NZRed << NHiggsRed << theFirstBeamMatcher << theSecondBeamMatcher << linefull << EvenNHiggs << BinomialNeutralBosons <<  Wmass << prefactor << proctype << colour_multiplicity_map << subsample << ProcessGeneration << NRandom << process_array << NumBosonParam << NumBosonParamA1 << NumBosonParamA2 << NumBosonParamB1 << NumBosonParamB2 << AdditionalPSFactors << LOME << ReinstatePSFactor << prob_Z << prob_W << prob_H << minphotons << minzs << minhiggses << BinomialGaugeBosons << BinomialHiggses << NWC_DAT_MAX << ounit(FreezeGaussianParamsScale, GeV) << FreezeGaussianParams << EnforceUnitarityLimit << ounit(EnforceUnitarityLimitScale, GeV);
} 

void MESphaleron::persistentInput(PersistentIStream& is, int) {
  is >> theNFamilies >> iunit(theSphaleronScale, GeV) >> iunit(theSphaleronScaleMax, GeV) >> theNAdditional >> theNAdditionalMin >> NPhotonMax >> NZMax >> NHiggsMax >> NPhotonRed >> NZRed >> NHiggsRed >> theFirstBeamMatcher >> theSecondBeamMatcher >> linefull >> EvenNHiggs >> BinomialNeutralBosons >> Wmass >> prefactor >> proctype >> colour_multiplicity_map >> subsample >> ProcessGeneration >> NRandom >> process_array >> NumBosonParam >> NumBosonParamA1 >> NumBosonParamA2 >> NumBosonParamB1 >> NumBosonParamB2 >> AdditionalPSFactors >> LOME >> ReinstatePSFactor >> prob_Z >> prob_W >> prob_H >> minphotons >> minzs >> minhiggses >> BinomialGaugeBosons >> BinomialHiggses >> NWC_DAT_MAX >> iunit(FreezeGaussianParamsScale, GeV) >> FreezeGaussianParams >> EnforceUnitarityLimit >> iunit(EnforceUnitarityLimitScale, GeV);
}


// *** Attention *** The following static variable is needed for the type
// description system in ThePEG. Please check that the template arguments
// are correct (the class and its base class), and that the constructor
// arguments are correct (the class name and the name of the dynamically
// loadable library where the class implementation can be found).
DescribeClass<MESphaleron,Herwig::BlobME>
  describeHerwigMESphaleron("Herwig::MESphaleron", "Sphalerons.so");

void MESphaleron::Init() {

  static Parameter<MESphaleron,size_t> interfaceNAdditionalMin
    ("NAdditionalMin",
     "The minimum number of additional objects to consider.",
     &MESphaleron::theNAdditionalMin, 0, 0, 0,
     false, false, Interface::lowerlim);

  static Parameter<MESphaleron,int> interfaceNPhotonMax
    ("NPhotonMax",
     "The maximum number of additional photons to consider.",
     &MESphaleron::NPhotonMax, -1, -1, 1000,
     false, false, Interface::lowerlim);

  static Parameter<MESphaleron,int> interfaceNHiggsMax
    ("NHiggsMax",
     "The maximum number of additional Higgsees to consider.",
     &MESphaleron::NHiggsMax, -1, -1, 1000,
     false, false, Interface::lowerlim);

  static Parameter<MESphaleron,int> interfaceNZMax
    ("NZMax",
     "The maximum number of additional Z bosons to consider.",
     &MESphaleron::NZMax, -1, -1, 1000,
     false, false, Interface::lowerlim);


    static Parameter<MESphaleron,int> interfaceNPhotonMin
    ("NPhotonMin",
     "The minimum number of additional photons to consider.",
     &MESphaleron::minphotons, 0, 0, 1000,
     false, false, Interface::lowerlim);

  static Parameter<MESphaleron,int> interfaceNHiggsMin
    ("NHiggsMin",
     "The maximum number of additional Higgsees to consider.",
     &MESphaleron::minhiggses, 0, 0, 1000,
     false, false, Interface::lowerlim);

  static Parameter<MESphaleron,int> interfaceNZMin
    ("NZMin",
     "The minimum number of additional Z bosons to consider.",
     &MESphaleron::minzs, 0, 0, 1000,
     false, false, Interface::lowerlim);

  static Parameter<MESphaleron, Energy> interfaceSphaleronScale
    ("SphaleronScale",
     "The Sphaleron Scale",
     &MESphaleron::theSphaleronScale, GeV, 9000.0*GeV, 200.0*GeV, 1.E20*GeV,
     false, false, Interface::limited);

    static Parameter<MESphaleron, Energy> interfaceSphaleronScaleMax
    ("SphaleronScaleMax",
     "The Maximum of the Sphaleron Scale",
     &MESphaleron::theSphaleronScaleMax, GeV, 80000.0*GeV, 200.0*GeV, 1.E20*GeV,
     false, false, Interface::limited);


    static Parameter<MESphaleron, double> interfacePrefactor
    ("Prefactor",
     "Prefactor for the cross section. ",
     &MESphaleron::prefactor, 1.6E-101,0., 1E150,
     false, false, Interface::limited);


    static Parameter<MESphaleron, double> interfaceNumBosonParamA1 
    ("NumBosonParamA1",
     "NumBosonParamA1",
     &MESphaleron::NumBosonParamA1,-21.497 ,-1.E99, 1.E99,
     false, false, Interface::limited);

    static Parameter<MESphaleron, double> interfaceNumBosonParamA2 
      ("NumBosonParamA2",
       "NumBosonParamA2",
       &MESphaleron::NumBosonParamA2,0.590E-2 ,-.1E99, 1.E99,
       false, false, Interface::limited);

    static Parameter<MESphaleron, double> interfaceNumBosonParamB1 
      ("NumBosonParamB1",
       "NumBosonParamB1",
       &MESphaleron::NumBosonParamB1,0.483E-1,-1.E99, 1.E99,
       false, false, Interface::limited);


    static Parameter<MESphaleron, double> interfaceNumBosonParamB2 
      ("NumBosonParamB2",
       "NumBosonParamB2",
       &MESphaleron::NumBosonParamB2, -0.854E-6,-1.E99, 1.E99,
       false, false, Interface::limited);

    
    static Switch<MESphaleron,bool> interfaceReinstatePSFactor
    ("ReinstatePSFactor",
     "Reinstate the phase-space factor related to the multiplicities (puts multiplicities on equal footing)",
     &MESphaleron::ReinstatePSFactor, false, false, false);
  static SwitchOption ReinstatePSFactorYes
    (interfaceReinstatePSFactor,
     "Yes",
     "Pre-multiply the ME by the phase space factor to put multiplicities on equal footing.",
     true);
  static SwitchOption ReinstatePSFactorNo
    (interfaceReinstatePSFactor,
     "No",
     "Do not reinstate the phase space factor related to multiplicities.",
     false);

  static Switch<MESphaleron,bool> interfaceFreezeGaussianParams
    ("FreezeGaussianParams",
     "Freeze the Gaussian multiplicity parameters beyond scale set by FreezeGaussianParamsScale.",
     &MESphaleron::FreezeGaussianParams, false, false, false);
  static SwitchOption FreezeGaussianParamsYes
    (interfaceFreezeGaussianParams,
     "Yes",
     "Freeze Gaussian multiplicity parameters.",
     true);
  static SwitchOption FreezeGaussianParamsNo
    (interfaceFreezeGaussianParams,
     "No",
     "DO NOT Freeze Gaussian multiplicity parameters.",
     false);

   static Parameter<MESphaleron, Energy> interfaceFreezeGaussianParamsScale
    ("FreezeGaussianParamsScale",
     "The scale to freeze the gaussian multiplicity parameters at",
     &MESphaleron::FreezeGaussianParamsScale, GeV, 15000.0*GeV, 200.0*GeV, 1.E20*GeV,
     false, false, Interface::limited);


    static Switch<MESphaleron,bool> interfaceEnforceUnitarityLimit
    ("EnforceUnitarityLimit",
     "Enforce the Unitarity Limit, i.e. make the partonic cross section scale as 1/sqrt(shat) above UnitarityLimitScale.",
     &MESphaleron::EnforceUnitarityLimit, false, false, false);
  static SwitchOption EnforceUnitarityLimitYes
    (interfaceEnforceUnitarityLimit,
     "Yes",
     "Enforce the Unitarity Limit (partonic cross section scale as 1/sqrt(shat)).",
     true);
  static SwitchOption EnforceUnitarityLimitNo
    (interfaceEnforceUnitarityLimit,
     "No",
     "DO NOT enforce the Unitarity Limit (partonic cross section flat).",
     false);

   static Parameter<MESphaleron, Energy> interfaceEnforceUnitarityLimitScale
    ("EnforceUnitarityLimitScale",
     "The scale at which to switch to ~1/shat",
     &MESphaleron::EnforceUnitarityLimitScale, GeV, 1.E4*GeV, 200.0*GeV, 1.E20*GeV,
     false, false, Interface::limited);
 
    static Switch<MESphaleron,bool> interfaceEvenNHiggs
    ("EvenNHiggs",
     "Forces the number of Higgs bosons to be even",
     &MESphaleron::EvenNHiggs, false, false, false);
  static SwitchOption EvenNHiggsYes
    (interfaceEvenNHiggs,
     "Yes",
     "The number of Higgs bosons is forced to be even.",
     true);
  static SwitchOption EvenNHiggsNo
    (interfaceEvenNHiggs,
     "No",
     "The number of Higgs bosons is NOT forced to be even.",
     false);

  static Switch<MESphaleron,bool> interfaceAdditionalPSFactors
    ("AdditionalPSFactors",
     "Forces the number of Higgs bosons to be even",
     &MESphaleron::AdditionalPSFactors, false, false, false);
  static SwitchOption AdditionalPSFactorsYes
    (interfaceAdditionalPSFactors,
     "Yes",
     "Include additional phase space factors a la Ringwald et al",
     true);
  static SwitchOption AdditionalPSFactorsNo
    (interfaceAdditionalPSFactors,
     "No",
     "Do not include additional PS factors",
     false);

    static Switch<MESphaleron,bool> interfaceLOME
    ("LOME",
     "Forces the number of Higgs bosons to be even",
     &MESphaleron::LOME, false, false, false);
  static SwitchOption LOMEYes
    (interfaceLOME,
     "Yes",
     "LOME a la Ringwald et al",
     true);
  static SwitchOption LOMENo
    (interfaceLOME,
     "No",
     "Do not use LOME",
     false);
  
  static Switch<MESphaleron,unsigned int> interfaceSubSampling
    ("SubSampling",
     "How many processes to sub-sample from total",
     &MESphaleron::subsample, 0, false, false);
  static SwitchOption interfaceSubSamplingAll
    (interfaceSubSampling,
     "All",
     "Register all processes for event generation",
     0);
  static SwitchOption interfaceSubSamplingOneRandom
    (interfaceSubSampling,
     "OneRandom",
     "Register one process from each multiplicity at random.",
     1);
  static SwitchOption interfaceSubSamplingNRandom
    (interfaceSubSampling,
     "NRandom",
     "Register N processes at random.",
     2);

    static Parameter<MESphaleron,size_t> interfaceNRandom
    ("NRandom",
     "If picking processes at random (SubSampling NRandom), how many to pick",
     &MESphaleron::NRandom, 0, 0, 0,
     false, false, Interface::lowerlim);

    static Switch<MESphaleron,unsigned int> interfaceProcessGeneration
    ("ProcessGeneration",
     "How to generate the processes",
     &MESphaleron::ProcessGeneration, 0, false, false);
  static SwitchOption interfaceProcessGenerationOnTheFly
    (interfaceProcessGeneration,
     "OnTheFly",
     "Generate all the processes via python script.",
     0);
  static SwitchOption interfaceProcessGenerationHardCoded
    (interfaceProcessGeneration,
     "HardCoded",
     "Use the hard-coded header file for the process generation.",
     1);

  static Switch<MESphaleron,unsigned int> interfaceProcessType
    ("ProcessType",
     "The process type (Sphaleron or Non-perturbative multi-gauge Boso)n",
     &MESphaleron::proctype, 0, false, false);
  static SwitchOption ProcessTypeSphaleron
    (interfaceProcessType,
     "Sphaleron",
     "Baryon/Lepton-number violating processes.",
     0);
  static SwitchOption ProcessTypeMultiGauge
    (interfaceProcessType,
     "MultiGauge",
     "Non-perturbative (baryon-conserving) multi-gauge boson processes.",
     1);
    static SwitchOption ProcessTypeSphaleronLight
    (interfaceProcessType,
     "SphaleronLight",
     "Baryon/Lepton-number violating processes without c-s-b quarks.",
     2);

    static Switch<MESphaleron,unsigned int> interfaceNumBosonParam
    ("NumBosonParam",
     "The parametrisation of the re-weighting of the gauge boson multiplcity",
     &MESphaleron::NumBosonParam, 0, false, false);
  static SwitchOption NumBosonParamFlat
    (interfaceNumBosonParam,
     "Flat",
     "Flat parametrisation: no explicit bias for any multiplicity",
     0);
  static SwitchOption NumBosonParamGaussian
    (interfaceNumBosonParam,
     "Gaussian",
     "Gaussian parametrisation, controlled by parameters NumBosonParamA, NumBosonParamB",
     1);
  static SwitchOption NumBosonParamGaussianUnnorm
    (interfaceNumBosonParam,
     "GaussianUnnorm",
     "Gaussian parametrisation (not normalised), controlled by parameters NumBosonParamA, NumBosonParamB",
     2);

  static Switch<MESphaleron,bool> interfaceBinomialNeutralGaugeBosons
    ("BinomialNeutralGaugeBosons",
     "Enables the number of Zs and gammas to be distributed according to a binomial distribution with p(Z) = cos^2 theta_W",
     &MESphaleron::BinomialNeutralBosons, true, false, false);
  static SwitchOption BinomialNeutralGaugeBosonsYes
    (interfaceBinomialNeutralGaugeBosons,
     "Yes",
     "The binomial distribution of neutral gauge bosons is enabled.",
     true);
  static SwitchOption BinomialNeutralGaugeBosonsNo
    (interfaceBinomialNeutralGaugeBosons,
     "No",
     "The binomial distribution of neutral gauge bosons is disabled.",
     false);

    static Switch<MESphaleron,bool> interfaceBinomialGaugeBosons
    ("BinomialGaugeBosons",
     "Enables the number of Ws and Zs to be distributed according to a binomial distribution with p(W) = ProbWvsZ interface (=2/3 default)",
     &MESphaleron::BinomialGaugeBosons, true, false, false);
  static SwitchOption BinomialGaugeBosonsYes
    (interfaceBinomialGaugeBosons,
     "Yes",
     "The binomial distribution of (charged vs neutral) gauge bosons is enabled.",
     true);
  static SwitchOption BinomialGaugeBosonsNo
    (interfaceBinomialGaugeBosons,
     "No",
     "The binomial distribution of (charged vs neutral) gauge bosons is disabled.",
     false);

  static Switch<MESphaleron,bool> interfaceBinomialHiggses
    ("BinomialHiggses",
     "Enables the number of Higgses to be distributed according to a binomial distribution with p(H) = ProbH interface (=1/16 default)",
     &MESphaleron::BinomialHiggses, true, false, false);
  static SwitchOption BinomialHiggsesYes
    (interfaceBinomialHiggses,
     "Yes",
     "The binomial distribution of (Higgs vs gauge) bosons is enabled.",
     true);
  static SwitchOption BinomialHiggsesNo
    (interfaceBinomialHiggses,
     "No",
     "The binomial distribution of (Higgs vs gauge) bosons is disabled.",
     false);


     static Parameter<MESphaleron, double> interfaceProbWvsZ
    ("ProbWvsZ",
     "Probability of W+/- vs Z",
     &MESphaleron::prob_W, 2/3 , 0., 1.,
     false, false, Interface::limited);
     
     static Parameter<MESphaleron, double> interfaceProbH
    ("ProbH",
     "Probability of Higgs vs gauge bosons",
     &MESphaleron::prob_H, 1/16, 0., 1.,
     false, false, Interface::limited);
 
  static ClassDocumentation<MESphaleron> documentation
    ("There is no documentation for the MESphaleron class");
}

