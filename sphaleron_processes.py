#!/usr/bin/python
import sys, os, random
from copy import deepcopy

def get_flavours(ntft, nd):    
    f_orig = [0] * ntft
    fall = [f_orig]
    for idm in range(nd):
        floop = deepcopy(fall)
        ftmp = []
        for ifdm in range(len(floop)):
            fnew = []
            ftarget = floop[ifdm]
            #print idm, ftarget
            for i in range(ntft):
                f0 = deepcopy(ftarget)
                if f0[i] == 0:
                    f0[i] = 1
                    if f0 not in ftmp: fnew.append(f0)
                    #print idm, ifdm, i, ftarget, f0
            ftmp += fnew
        fall = ftmp
    return fall

def get_charge(p):
    uplist = ['u1','u2','u3','c1','c2','c3','t1','t2','t3']
    downlist = ['d1','d2','d3','s1','s2','s3','b1','b2','b3']
    nulist = ['ne','nm','nt']
    elist = ['e','m','tau']
    ubarlist = ['u1bar','u2bar','u3bar']
    dbarlist = ['d1bar','d2bar','d3bar']
    if p in uplist: return 2
    if p in downlist: return -1
    if p in nulist: return 0
    if p in elist: return -3
    if p in ubarlist: return -2
    if p in dbarlist: return 1
    n, s = p.split('w')
    if s == '+': return  3 * int(n)
    if s == '-': return -3 * int(n)

def calc_net_charge(proc):
    net = 0
    for p in proc.split():
        net += get_charge(p)
    return net

#######################################################
#######################################################

#try:
#    nw = int(sys.argv[1])
#except:
#    print '[number of W bosons]'
#    exit()

def str_to_pdg(string_in):
    #print(string_in)
    if string_in == '->' or string_in == '':
        return ''
    part_sign = 1
    numws = 0
    id = -999
    if 'd1' in string_in or 'd2' in string_in or 'd3' in string_in:
        id = 1
    if 'u1' in string_in or 'u2' in string_in or 'u3' in string_in:
        id = 2
    if 's1' in string_in or 's2' in string_in or 's3' in string_in:
        id = 3
    if 'c1' in string_in or 'c2' in string_in or 'c3' in string_in:
        id = 4
    if 'b1' in string_in or 'b2' in string_in or 'b3' in string_in:
        id = 5
    if 't1' in string_in or 't2' in string_in or 't3' in string_in:
        id = 6
    if 'w' in string_in:
        id = 24
        if string_in[0] != 'w': 
            numws = int(float((string_in.strip('w+').strip('w-'))))
            #print string_in, numws
    if 'm' in string_in and 'nm' not in string_in:
        id = 13
    if 'e' in string_in and 'ne' not in string_in:
        id = 11
    if 'tau' in string_in:
        id = 15
    if 'nm' in string_in:
        id = 14
    if 'ne' in string_in:
        id = 12
    if 'nt' in string_in:
        id = 16
    if 'bar' in string_in or '-' in string_in:
        part_sign = -1
    if numws == 0:
        return str(id*part_sign)
    else:
        output_str = ''
        for i in range(0,numws):
            output_str = output_str + ' ' + str(id*part_sign)
        return output_str
    
def proc_to_pdg(process_in):
    proc_to_pdg_res = ''
    split_process = process_in.split()
    for ss in split_process:
       # print ss
        #print str_to_pdg(ss)
        proc_to_pdg_res = proc_to_pdg_res + ' ' + str_to_pdg(ss)
        #print proc_to_pdg_res
    return proc_to_pdg_res

#print 'u1bar d2bar -> d3 c1 s2 c3 t1 b2 b3 e m tau 2w+', proc_to_pdg('u1bar d2bar -> d3 c1 s2 c3 t1 b2 b3 e m tau 2w+')


def sort_process(process_in):
    # first convert to PDG ids
    process_in_pdg = proc_to_pdg(process_in)
    particle_list = process_in_pdg.split()
    particle_list_int = [int(x) for x in particle_list]
    particle_list_int.sort()
    return particle_list_int

#print sort_process('u1bar d2bar -> d3 c1 s2 c3 t1 b2 b3 e m tau 2w+')


def include_process(process_in, process_list_current):
    if process_in in process_list_current:
        #print 'process', process_in, 'already included'
        return False
    else:
        return True



def proc_to_string(process_in):
    process_in_string = ''
    for ss in process_in:
        process_in_string = process_in_string + str(ss)
    #print('process_in_string=', process_in_string)
    return process_in_string
    
    
#nw = 30
def process_string(nw):
    if nw % 2 == 0: dnwmax = min([nw, 6])
    if nw % 2 == 1: dnwmax = min([nw, 5])
        
    tft  = [('u1','d1'),('u2','d2'),('u3','d3')]
    tft += [('c1','s1'),('c2','s2'),('c3','s3')]
    tft += [('t1','b1'),('t2','b2'),('t3','b3')]
    tft += [('ne','e'),('nm','m'),('nt','tau')]

    proc_all = []
    proc_all_sorted = []
    proc_multiplicity = {}
    proc_pdg_to_sorted_dict = {}
    for dnw in range(-dnwmax, dnwmax+1, 2):
        nwp = (nw + dnw)/2
        nwm = (nw - dnw)/2
        nu = 6 - dnw
        nd = 12 - nu
        #print nwp, nwm, dnw, nw
        #continue
        flavours = get_flavours(12, nd)
        #print flavours
        for flavour in flavours:
            fermions = ''
            for i in range(12):
                fdm = flavour[i]
                fermions += tft[i][fdm]
                if i == 0: fermions += 'bar '
                if i == 1: fermions += 'bar -> '
                if i > 1:  fermions += ' '
            proc = fermions
            #print nwp, nwm
            if nwp > 0:
                proc += str(nwp) + 'w+ ' 
            if nwm > 0:
                proc += str(nwm) + 'w- '
            #print proc
            proc_sorted = sort_process(proc)
            #if include_process(proc_sorted, proc_all_sorted) is True:
            if include_process(proc_sorted, proc_all_sorted) is True:
                proc_multiplicity[proc_to_string(proc_sorted)] = 1
                #print 'adding', proc
                proc_all_sorted.append(proc_sorted)
                proc_all.append(proc_to_pdg(proc))
                proc_pdg_to_sorted_dict[proc_to_pdg(proc)] = proc_to_string(proc_sorted)
            else:
                proc_multiplicity[proc_to_string(proc_sorted)] = proc_multiplicity[proc_to_string(proc_sorted)] + 1
                
    #print proc_multiplicity
    #count_processes = 0.
    #for p in proc_all_sorted:
    #    i = 0
    #    while abs(p[i]) == 24:
    #        i = i+1
    #    #print 'p[i],p[i+1]', p[i],p[i+1]
    #    if p[i] == p[i+1] and proc_multiplicity[proc_to_string(p)]%2 == 0:
    #        print 'COUNTEREXAMPLE'
    #        print p
        #print p, proc_multiplicity[proc_to_string(p)]
    #    count_processes = count_processes + proc_multiplicity[proc_to_string(p)]
    #print count_processes
    # 
    # add the colour-configuration multiplicity as the first part of the string (for ease of implementation):
    proc_all_multiplicity = []
    for pp in proc_all:
        #print pp
        qq = proc_pdg_to_sorted_dict[pp]
        #print proc_multiplicity[qq], pp
        if proc_multiplicity[qq]%2 == 0:
            divfac = 2.
        else:
            divfac = 1.
        proc_mult = str(proc_multiplicity[qq]/divfac) + ' ' + pp
        proc_all_multiplicity.append(proc_mult)
    #print proc_all_multiplicity
    return proc_all_multiplicity

def process_string_BC(nw):

    data = {}
    data['u1bar u1bar -> u1bar u1bar'] = {'col_multi': 1, 'dnw':  0}
    data['u1bar u1bar -> u1bar d1bar'] = {'col_multi': 1, 'dnw':  -1}
    data['u1bar u1bar -> d1bar d1bar'] = {'col_multi': 1, 'dnw':  -2}

    data['d1bar d1bar -> u1bar u1bar'] = {'col_multi': 1, 'dnw': 2}
    data['d1bar d1bar -> u1bar d1bar'] = {'col_multi': 1, 'dnw': 1}
    data['d1bar d1bar -> d1bar d1bar'] = {'col_multi': 1, 'dnw': 0}

    data['u1bar d1bar -> u1bar u1bar'] = {'col_multi': 1, 'dnw': 1}
    data['u1bar d1bar -> u1bar d1bar'] = {'col_multi': 2, 'dnw':  0}
    data['u1bar d1bar -> d1bar d1bar'] = {'col_multi': 1, 'dnw': -1}

    proc_all, proc_all_sorted = [], []
    for proc in list(data.keys()):
        col_multi = data[proc]['col_multi']
        dnw = data[proc]['dnw']
        if (nw + dnw) % 2 != 0: continue
        nwp = (nw + dnw)/2
        nwm = (nw - dnw)/2
        if nwp > 0:
            proc += ' ' + str(nwp) + 'w+' 
        if nwm > 0:
            proc += ' ' + str(nwm) + 'w-'
        #print proc, col_multi
        proc_sorted = sort_process(proc)
        if include_process(proc_sorted, proc_all_sorted) is True:
            #print 'adding', proc
            proc_all_sorted.append(proc_sorted)
            proc_all.append(str(col_multi) + ' ' + proc_to_pdg(proc))
    #print proc_all
    return proc_all

   # for proc in proc_all:
   #     initial, final = proc.split('->')
   #     in_charge = calc_net_charge(initial)
   #     out_charge = calc_net_charge(initial)
   #     diff = in_charge - out_charge
    #print proc_all
        #print proc, '|', in_charge, out_charge, diff

#print process_string_BC(2)


#nw = 30
def process_string_light(nw):
    if nw % 2 == 0: dnwmax = min([nw, 6])
    if nw % 2 == 1: dnwmax = min([nw, 5])
        
    tft  = [('u1','d1'),('u2','d2'),('u3','d3')]
    tft += [('u1','d1'),('u2','d2'),('u3','d3')]
    tft += [('t1','d1'),('t2','d2'),('t3','d3')]
    tft += [('ne','e'),('nm','m'),('nt','tau')]

    proc_all = []
    proc_all_sorted = []
    proc_multiplicity = {}
    proc_pdg_to_sorted_dict = {}
    for dnw in range(-dnwmax, dnwmax+1, 2):
        nwp = (nw + dnw)/2
        nwm = (nw - dnw)/2
        nu = 6 - dnw
        nd = 12 - nu
        #print nwp, nwm, dnw, nw
        #continue
        flavours = get_flavours(12, nd)
        for flavour in flavours:
            fermions = ''
            for i in range(12):
                fdm = flavour[i]
                fermions += tft[i][fdm]
                if i == 0: fermions += 'bar '
                if i == 1: fermions += 'bar -> '
                if i > 1:  fermions += ' '
            proc = fermions
            #print nwp, nwm
            if nwp > 0:
                proc += str(nwp) + 'w+ ' 
            if nwm > 0:
                proc += str(nwm) + 'w- '
            #print proc
            proc_sorted = sort_process(proc)
            #if include_process(proc_sorted, proc_all_sorted) is True:
            if include_process(proc_sorted, proc_all_sorted) is True:
                proc_multiplicity[proc_to_string(proc_sorted)] = 1
                #print 'adding', proc
                proc_all_sorted.append(proc_sorted)
                proc_all.append(proc_to_pdg(proc))
                proc_pdg_to_sorted_dict[proc_to_pdg(proc)] = proc_to_string(proc_sorted)
            else:
                proc_multiplicity[proc_to_string(proc_sorted)] = proc_multiplicity[proc_to_string(proc_sorted)] + 1
                
    #print proc_multiplicity
    #count_processes = 0.
    #for p in proc_all_sorted:
    #    i = 0
    #    while abs(p[i]) == 24:
    #        i = i+1
    #    #print 'p[i],p[i+1]', p[i],p[i+1]
    #    if p[i] == p[i+1] and proc_multiplicity[proc_to_string(p)]%2 == 0:
    #        print 'COUNTEREXAMPLE'
    #        print p
        #print p, proc_multiplicity[proc_to_string(p)]
    #    count_processes = count_processes + proc_multiplicity[proc_to_string(p)]
    #print count_processes
    # 
    # add the colour-configuration multiplicity as the first part of the string (for ease of implementation):
    proc_all_multiplicity = []
    for pp in proc_all:
        #print pp
        qq = proc_pdg_to_sorted_dict[pp]
        #print proc_multiplicity[qq], pp
        if proc_multiplicity[qq]%2 == 0:
            divfac = 2.
        else:
            divfac = 1.
        proc_mult = str(proc_multiplicity[qq]/divfac) + ' ' + pp
        proc_all_multiplicity.append(proc_mult)
    #print proc_all_multiplicity
    return proc_all_multiplicity


#PyModule_GetDictprocess_string(100)


